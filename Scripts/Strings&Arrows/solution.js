//Подсказка: сюда можно складывать записи адресной книги.
var phoneBook = [];

//Здесь можно объявить переменные и функции, которые понядобятся вам для работы ваших функций
function add(data) {
    var regExp = new RegExp("  ", 'g');
    var record = data.replace(/[,]/g, " ").replace(regExp, " ").split(" "),
        name = record[0],
        checkName = search(name),
        numbers = "";
    for (var index = 1; index < record.length - 1; index++) {
        numbers += " " + record[index] + ",";
    }
    numbers += " " + record[record.length - 1];
    if (checkName >= 0 && phoneBook[checkName].length > name.length + 1) {
        phoneBook[checkName] += "," + numbers;
    } else if (checkName >= 0 && phoneBook[checkName].length == name.length + 1) {
        phoneBook[checkName] += numbers;
    } else {
        phoneBook.push(name + ":" + numbers);
    }
}

function search(name) {
    var buffer;
    var result = -1;
    phoneBook.forEach(function (phone, index) {
        buffer = phone.substring(0, name.length);
        if (buffer === name) result = index;
    });
    return result;
}

function remove(phone) {
    var result = false;
    for (var recordIndex = 0; recordIndex < phoneBook.length; recordIndex++) {
        var index = phoneBook[recordIndex].indexOf(" " + phone);
        if (index >= 0) {
            if (phoneBook[recordIndex].indexOf(",") < 0) {
                phoneBook[recordIndex] = phoneBook[recordIndex].replace(" " + phone, "");
            } else if (phoneBook[recordIndex].indexOf(",", index) < 0) {
                phoneBook[recordIndex] = phoneBook[recordIndex].replace(", " + phone, "");
            } else {
                phoneBook[recordIndex] = phoneBook[recordIndex].replace(phone + ", ", "");
            }
            result = true;
            break;
        }
    }
    return result;
}

function show() {
    for (var index = 0; index<phoneBook.length; index++) {
        if (phoneBook[index].length<=phoneBook[index].indexOf(":")+1) {
            phoneBook.splice(index,1);
        }
    }
    return phoneBook.sort();
}

//

module.exports = {
    getWords: function (sentence) {
        var words = sentence.split(" ");
        var result = [];
        words.forEach(function (word) {
            if (word[0] === '#') result.push(word.substring(1));
        });
        return result;
    },
    normalizeWords: function (words) {
        var result = words.map(function (word) {
            return word.toLowerCase();
        }).filter(function (word, index, source) {
            return source.indexOf(word) === index;
        });
        return result.join(', ');
    },
    addressBook: function (command) {
        var words = command.split(" "),
            action = words[0],
            data = "";
        for (var index = 1; index < words.length - 1; index++) {
            data += words[index] + " ";
        }
        data += words[words.length - 1];
        switch (action) {
            case "ADD": {
                add(data);
                break;
            }
            case "REMOVE_PHONE": {
                return remove(words[1]);
                break;
            }
            case "SHOW": {
                return show();
                break;
            }
            default:
                return "Не верный формат команды";
        }
    }
};
