var options = {
    year: 'numeric',
    month: 'numeric',
    day: 'numeric',
    hour: 'numeric',
    minute: 'numeric',
};
var options2 = {
    hour: 'numeric',
    minute: 'numeric',
};


function timeShift(date) {
    return {
        date: new Date(date),
        value: "",
        add: function (number, unit) {
            switch (unit) {
                case 'years':
                    this.date = new Date(this.date.setFullYear(this.date.getFullYear() + number));
                     break;
                case 'months':
                    this.date = new Date(this.date.setMonth(this.date.getMonth() + number));
                    break;
                case 'days':
                    this.date = new Date(this.date.setDate(this.date.getDate() + number));
                    break;
                case 'hours':
                    this.date = new Date(this.date.setHours(this.date.getHours() + number));
                    break;
                case 'minutes':
                    this.date = new Date(this.date.setMinutes(this.date.getMinutes() + number));
                    break;
                default:
                    return 'Err cmd';

            }
            this.value = this.date.getFullYear()+"-"+
                (this.date.getMonth() < 10 ? "0"+(this.date.getMonth()+1) : this.date.getMonth()+1)+"-"+
                (this.date.getDate() < 10 ? "0"+this.date.getDate() : this.date.getDate())+" "+
                (this.date.getHours() < 10 ? "0"+this.date.getHours() : this.date.getHours())+":"+
                (this.date.getMinutes() < 10 ? "0"+this.date.getMinutes() : this.date.getMinutes());
            return this;
        },
        substract: function (number, unit) {
            this.add(-number,unit);
            return this;
        }
    }
}

var date = timeShift("2017-05-23 16:42");

/*
function query(collection) {

}

function select() {

}

function filterIn(property, values) {
}

*/


var friends = [
    {
        name: 'Ivan',
        gender: 'Man',
        age: '42',
        favoriteFood: 'Картофель'
    },
    {
        name: 'Inna',
        gender: 'Woman',
        age: '33',
        favoriteFood: 'Булочка'
    },
    {
        name: 'Vladimir',
        gender: 'Man',
        age: '33',
        favoriteFood: 'Булочка'
    }
];

var lib = {
    query: function (collection) {
        var copyCollection = collection.slice();
        for (var tasks = 1; tasks < arguments.length; tasks++)
            if (arguments[tasks].name == 'filterIn') {
                copyCollection = arguments[tasks].action(copyCollection);
            }
        for (var tasks = 1; tasks < arguments.length; tasks++)
            if (arguments[tasks].name == 'select') {
                copyCollection = arguments[tasks].action(copyCollection);
            }
        return copyCollection;
    },
    select: function () {
        var args1 = arguments;
        var buff = {};
        var rez = [];
        return {
            name: 'select',
            action: function (item) {
                var args = args1;
                for (record in item) {
                    for (fieldName in item[record]) {
                        for (var selectId = 0; selectId < args1.length; selectId++) {
                            if (args1[selectId] == fieldName) {
                                buff[fieldName] = item[record][fieldName];
                            }
                        }
                    }
                    rez.push(buff);
                    buff = {};
                }
                return rez;
            }
        }
    },
    filterIn: function (property, values) {
        var args1 = arguments;
        var rez = [];

        return {
            name: 'filterIn',
            action: function (item) {
                for (record in item) {
                    for (fieldName in item[record]) {
                        if (fieldName == property) {
                            for (var key = 0; key <= values.length - 1; key++) {
                                if (item[record][fieldName] == values[key]) {
                                    rez.push(item[record]);
                                }
                            }
                        }
                    }
                }
                return rez;
            }
        }
    }
};