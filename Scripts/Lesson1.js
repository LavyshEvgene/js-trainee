var numb = 3,
    str = 'Hello World!',
    check = false,
    obj =  {},
    undfnd,
    x = null;

console.log(typeof numb);
console.log(typeof str);
console.log(typeof check);
console.log(typeof obj);
console.log(typeof undfnd);
console.log(typeof x );


console.log(123);
console.log(typeof (123));
console.log('yes');
console.log(typeof ('yes'));
console.log(false);
console.log(typeof (false));
console.log({});
console.log(typeof ({}));
console.log(null);
console.log(typeof (null));
console.log(undefined);
console.log(typeof (undefined));

console.log(typeof 2);