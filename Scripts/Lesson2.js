var str = String(null),
    str2 = str + 13,
    numb = '301923';

console.log('Строковое преобразование');
console.log(str=='null');
console.log(str2);
console.log('-----------------------------');
console.log('');

console.log('Числовое преобразование');
console.log(numb);
console.log(typeof numb);
console.log(typeof +numb);
console.log(Number(true));
console.log(Number('    '));
console.log(Number(' 302'));
console.log(Number('13 23 15'));
console.log('-----------------------------');
console.log('');

console.log('Логическое преобразование');
console.log(!!0);
console.log(!!1);
console.log(!!'  ');
console.log(!!'');
console.log(!!'123');
console.log(!!undefined);
console.log(!!NaN);
console.log(!!{});
console.log('-----------------------------');
console.log('');